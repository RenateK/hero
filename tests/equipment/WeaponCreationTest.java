package equipment;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WeaponCreationTest {
    @Test
    public void createWeapon_validInputs_shouldReturnName(){
        //Arrange
        String expected = "Crazy Axy";
        Weapon weapon = new Weapon("Crazy Axy", WeaponType.AXE, 10, 20);
        //Act
        String actual = weapon.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createWeapon_validInputs_shouldReturnRequiredLevel(){
        //Arrange
        int expected = 5;
        //Act
        Weapon weapon = new Weapon("Bob the sword", WeaponType.SWORD, 5, 2);
        int actual = weapon.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createWeapon_validInputs_shouldReturnSlot(){
        //Arrange
        Slot expected = Slot.WEAPON;
        Weapon weapon = new Weapon("Crazy Axy", WeaponType.AXE, 2, 1);
        //Act
        Slot actual = weapon.getSlot();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createWeapon_validInputs_shouldReturnWeaponType(){
        //Arrange
        Weapon weapon = new Weapon("Douche Dagg", WeaponType.DAGGER, 10, 20);
        WeaponType expected = WeaponType.DAGGER;
        //Act
        WeaponType actual = weapon.getWeaponType();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createWeapon_validInputs_shouldReturnWeaponDamage(){
        //Arrange
        int expected = 14;
        Weapon weapon = new Weapon("Fancy Stick", WeaponType.STAFF, 2, 14);
        //Act
        int actual = weapon.getWeaponDamage();
        //Assert
        assertEquals(expected, actual);
    }
}
