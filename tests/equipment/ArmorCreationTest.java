package equipment;

import hero.HeroAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArmorCreationTest {
    @Test
    public void createArmor_validInputs_shouldReturnName(){
        //Arrange
        String expected = "Leather poncho";
        Armor armor = new Armor("Leather poncho", ArmorType.LEATHER, 2, Slot.BODY, new HeroAttributes(1,10,1));
        //Act
        String actual = armor.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createArmor_validInputs_shouldReturnRequiredLevel(){
        //Arrange
        int expected = 5;
        //Act
        Armor armor = new Armor("Cool and stiff pants", ArmorType.PLATE, 5, Slot.LEGS, new HeroAttributes(5,2,1));
        int actual = armor.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createArmor_validInputs_shouldReturnSlot(){
        //Arrange
        Slot expected = Slot.HEAD;
        Armor armor = new Armor("Really clanky hat", ArmorType.MAIL, 2, Slot.HEAD, new HeroAttributes(10,5,1));
        //Act
        Slot actual = armor.getSlot();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createArmor_validInputs_shouldReturnArmorType(){
        //Arrange
        ArmorType expected = ArmorType.CLOTH;
        Armor armor = new Armor("Stealthy Nightcap", ArmorType.CLOTH, 10, Slot.HEAD, new HeroAttributes(1, 20, 5));
        //Act
        ArmorType actual = armor.getArmorType();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void createArmor_validInputs_shouldReturnArmorAttributes(){
        //Arrange
        int expectedStrength = 2;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;
        Armor armor = new Armor("Fancy Stick", ArmorType.PLATE, 2, Slot.HEAD, new HeroAttributes(2,4,2));
        //Act
        int actualStrength = armor.getArmorAttribute().getStrength();
        int actualDexterity = armor.getArmorAttribute().getDexterity();
        int actualIntelligence = armor.getArmorAttribute().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }
}
