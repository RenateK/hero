package hero;

import equipment.*;
import exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WarriorTest {

    @Test
    public void warriorLevelUp_validInputs_shouldReturnIncreasedLevel() {
        //Arrange
        int expected = 2;
        Hero warrior = new Warrior("Renate");
        //Act
        warrior.levelUp();
        int actual = warrior.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void warrior_validInputs_shouldReturnHeroAttributes(){
        //Arrange
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;
        Hero warrior = new Warrior("Renate");
        //Act
        int actualStrength = warrior.getLevelAttributes().getStrength();
        int actualDexterity = warrior.getLevelAttributes().getDexterity();
        int actualIntelligence = warrior.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    @Test
    public void warriorLevelUp_validInputs_shouldReturnIncreasedHeroAttributes(){
        //Arrange
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;
        Hero warrior = new Warrior("Renate");
        //Act
        warrior.levelUp();
        int actualStrength = warrior.getLevelAttributes().getStrength();
        int actualDexterity = warrior.getLevelAttributes().getDexterity();
        int actualIntelligence = warrior.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    //Equipping Armor
    @Test
    public void warriorEquipArmor_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero warrior = new Warrior("Renate");
            Armor armor = new Armor("Shiny Armor", ArmorType.PLATE, 3,
                    Slot.BODY, new HeroAttributes(1,2,3));
            warrior.equipArmor(armor);
        });
    }

    @Test
    public void warriorEquipArmor_validLevel_shouldNotThrowException() {
        try{
            Hero warrior = new Warrior("Renate");
            Armor armor = new Armor("Shiny Armor", ArmorType.PLATE, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            warrior.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void warriorEquipArmor_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero warrior = new Warrior("Renate");
            Armor armor = new Armor("Frantic Hoodie", ArmorType.CLOTH, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            warrior.equipArmor(armor);
        });
    }

    @Test
    public void warriorEquipArmor_validType_shouldNotThrowException() {
        try{
            Hero warrior = new Warrior("Renate");
            Armor armor = new Armor("Shiny Armor", ArmorType.PLATE, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            warrior.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    //Equipping weapon
    @Test
    public void warriorEquipWeapon_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero warrior = new Warrior("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.AXE, 3, 10);
            warrior.equipWeapon(weapon);
        });
    }

    @Test
    public void warriorEquipWeapon_validLevel_shouldNotThrowException() {
        try{
            Hero warrior = new Warrior("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.AXE, 1, 10);
            warrior.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void warriorEquipWeapon_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero warrior = new Warrior("Renate");
            Weapon weapon = new Weapon("BoooOw", WeaponType.BOW, 1, 10);
            warrior.equipWeapon(weapon);
        });
    }

    @Test
    public void warriorEquipWeapon_validType_shouldNotThrowException() {
        try{
            Hero warrior = new Warrior("Renate");
            Weapon weapon = new Weapon("Scary Axe", WeaponType.AXE, 1, 10);
            warrior.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }
}
