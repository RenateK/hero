package hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    //When a Hero is created, it needs to have the correct name, level, and attributes.
    @Test
    public void hero_validInputs_shouldReturnCorrectName() {
        //Arrange
        String expected = "Renate";
        Hero mage = new Mage("Renate");
        //Act
        String actual = mage.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void hero_validInputs_shouldReturnCorrectLevel() {
        //Arrange
        int expected = 1;
        Hero mage = new Mage("Renate");
        //Act
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }



    @Test
    public void heroLevelUp_validInputs_shouldReturnCorrectAttributes() {
        //Arrange
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        Hero mage = new Mage("Renate");
        //Act
        int actualStrength = mage.getLevelAttributes().getStrength();
        int actualDexterity = mage.getLevelAttributes().getDexterity();
        int actualIntelligence = mage.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    @Test
    public void levelUp_validInputs_shouldReturnIncreasedLevel(){
        //Arrange
        int expected = 2;
        Hero mage = new Mage("Renate");
        //Act
        mage.levelUp();
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void displayHero_shouldReturnCorrectState(){
        //Arrange
        Hero mage = new Mage("Renate");
        StringBuilder stb = new StringBuilder();
        stb.append("Name: ");
        stb.append("Renate");
        stb.append("\n");
        stb.append("Class: ");
        stb.append("Mage");
        stb.append("\n");
        stb.append("Level: ");
        stb.append(1);
        stb.append("\n");
        stb.append("Strength: ");
        stb.append(1);
        stb.append("\n");
        stb.append("Dexterity: ");
        stb.append(1);
        stb.append("\n");
        stb.append("Intelligence: ");
        stb.append(8);
        stb.append("\n");
        stb.append("Damage: ");
        stb.append(1.0);
        String expected = stb.toString();
        //Act
        String actual = mage.displayHero();
        //Assert
        assertEquals(expected, actual);
    }

}
