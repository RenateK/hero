package hero;

import equipment.*;
import exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MageTest {

    @Test
    public void mageLevelUp_validInputs_shouldReturnIncreasedLevel() {
        //Arrange
        int expected = 2;
        Hero mage = new Mage("Renate");
        //Act
        mage.levelUp();
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void mage_validInputs_shouldReturnHeroAttributes(){
        //Arrange
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        Hero mage = new Mage("Renate");
        //Act
        int actualStrength = mage.getLevelAttributes().getStrength();
        int actualDexterity = mage.getLevelAttributes().getDexterity();
        int actualIntelligence = mage.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    @Test
    public void mageLevelUp_validInputs_shouldReturnIncreasedHeroAttributes(){
        //Arrange
        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedIntelligence = 13;
        Hero mage = new Mage("Renate");
        //Act
        mage.levelUp();
        int actualStrength = mage.getLevelAttributes().getStrength();
        int actualDexterity = mage.getLevelAttributes().getDexterity();
        int actualIntelligence = mage.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    //Equipping Armor
    @Test
    public void mageEquipArmor_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero mage = new Mage("Renate");
            Armor armor = new Armor("Frantic Hoodie", ArmorType.CLOTH, 3,
                    Slot.BODY, new HeroAttributes(1,2,3));
            mage.equipArmor(armor);
        });
    }

    @Test
    public void mageEquipArmor_validLevel_shouldNotThrowException() {
        try{
            Hero mage = new Mage("Renate");
            Armor armor = new Armor("mmm cloth", ArmorType.CLOTH, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            mage.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void mageEquipArmor_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero mage = new Mage("Renate");
            Armor armor = new Armor("Frantic Hoodie", ArmorType.PLATE, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            mage.equipArmor(armor);
        });
    }

    @Test
    public void mageEquipArmor_validType_shouldNotThrowException() {
        try{
            Hero mage = new Mage("Renate");
            Armor armor = new Armor("mmm cloth", ArmorType.CLOTH, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            mage.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    //Equipping weapon
    @Test
    public void mageEquipWeapon_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero mage = new Mage("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.WAND, 3, 10);
            mage.equipWeapon(weapon);
        });
    }

    @Test
    public void mageEquipWeapon_validLevel_shouldNotThrowException() {
        try{
            Hero mage = new Mage("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.WAND, 1, 10);
            mage.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void mageEquipWeapon_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero mage = new Mage("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.HAMMER, 1, 10);
            mage.equipWeapon(weapon);
        });
    }

    @Test
    public void mageEquipWeapon_validType_shouldNotThrowException() {
        try{
            Hero mage = new Mage("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.WAND, 1, 10);
            mage.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }
}
