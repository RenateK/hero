package hero;

import equipment.*;
import exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RogueTest {

    @Test
    public void rogueLevelUp_validInputs_shouldReturnIncreasedLevel() {
        //Arrange
        int expected = 2;
        Hero rogue = new Rogue("Renate");
        //Act
        rogue.levelUp();
        int actual = rogue.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void rogue_validInputs_shouldReturnHeroAttributes(){
        //Arrange
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;
        Hero rogue = new Rogue("Renate");
        //Act
        int actualStrength = rogue.getLevelAttributes().getStrength();
        int actualDexterity = rogue.getLevelAttributes().getDexterity();
        int actualIntelligence = rogue.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    @Test
    public void rogueLevelUp_validInputs_shouldReturnIncreasedHeroAttributes(){
        //Arrange
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;
        Hero rogue = new Rogue("Renate");
        //Act
        rogue.levelUp();
        int actualStrength = rogue.getLevelAttributes().getStrength();
        int actualDexterity = rogue.getLevelAttributes().getDexterity();
        int actualIntelligence = rogue.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    //Equipping Armor
    @Test
    public void rogueEquipArmor_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero rogue = new Rogue("Renate");
            Armor armor = new Armor("Nice biker", ArmorType.LEATHER, 3,
                    Slot.BODY, new HeroAttributes(1,2,3));
            rogue.equipArmor(armor);
        });
    }

    @Test
    public void rogueEquipArmor_validLevel_shouldNotThrowException() {
        try{
            Hero rogue = new Rogue("Renate");
            Armor armor = new Armor("mmm leather", ArmorType.LEATHER, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            rogue.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void rogueEquipArmor_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero rogue = new Rogue("Renate");
            Armor armor = new Armor("Frantic Hoodie", ArmorType.CLOTH, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            rogue.equipArmor(armor);
        });
    }

    @Test
    public void rogueEquipArmor_validType_shouldNotThrowException() {
        try{
            Hero rogue = new Rogue("Renate");
            Armor armor = new Armor("mmm leather", ArmorType.LEATHER, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            rogue.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    //Equipping weapon
    @Test
    public void rogueEquipWeapon_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero rogue = new Rogue("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.SWORD, 3, 10);
            rogue.equipWeapon(weapon);
        });
    }

    @Test
    public void rogueEquipWeapon_validLevel_shouldNotThrowException() {
        try{
            Hero rogue = new Rogue("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.DAGGER, 1, 10);
            rogue.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void rogueEquipWeapon_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero rogue = new Rogue("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.HAMMER, 1, 10);
            rogue.equipWeapon(weapon);
        });
    }

    @Test
    public void rogueEquipWeapon_validType_shouldNotThrowException() {
        try{
            Hero rogue = new Rogue("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.DAGGER, 1, 10);
            rogue.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }
}
