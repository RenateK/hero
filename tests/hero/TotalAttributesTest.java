package hero;

import equipment.Armor;
import equipment.ArmorType;
import equipment.Slot;
import exceptions.InvalidArmorException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TotalAttributesTest {
    @Test
    public void totalAttributes_noEquipment_shouldReturnCorrectList() {
        Hero mage = new Mage("Renate");
        List<Float> expected = new ArrayList<>();
        expected.add(1.0F);
        expected.add(1.0F);
        expected.add(8.0F);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void totalAttributes_OneArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Renate");
        Armor armor = new Armor("Cozy Cloth", ArmorType.CLOTH, 1,
                Slot.BODY, new HeroAttributes(1,2,3));
        mage.equipArmor(armor);
        List<Float> expected = new ArrayList<>();
        expected.add(2.0F);
        expected.add(3.0F);
        expected.add(11.0F);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void totalAttributes_TwoArmors_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Renate");
        Armor armor = new Armor("Cozy Cloth", ArmorType.CLOTH, 1,
                Slot.BODY, new HeroAttributes(1,2,3));
        Armor anotherArmor = new Armor("Rude Cloth", ArmorType.CLOTH, 1,
                Slot.HEAD, new HeroAttributes(2,4,1));
        mage.equipArmor(armor);
        mage.equipArmor(anotherArmor);
        List<Float> expected = new ArrayList<>();
        expected.add(4.0F);
        expected.add(7.0F);
        expected.add(12.0F);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void totalAttributes_ReplaceArmor_shouldReturnCorrectList() throws InvalidArmorException {
        Hero mage = new Mage("Renate");
        Armor armor = new Armor("Cozy Cloth", ArmorType.CLOTH, 1,
                Slot.BODY, new HeroAttributes(1,2,3));
        Armor anotherArmor = new Armor("Rude Cloth", ArmorType.CLOTH, 1,
                Slot.BODY, new HeroAttributes(2,4,1));
        mage.equipArmor(armor);
        mage.equipArmor(anotherArmor);
        List<Float> expected = new ArrayList<>();
        expected.add(3.0F);
        expected.add(5.0F);
        expected.add(9.0F);
        List<Float> actual = mage.totalAttributes();

        assertEquals(expected, actual);
    }
}
