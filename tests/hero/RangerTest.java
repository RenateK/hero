package hero;

import equipment.*;
import exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RangerTest {

    @Test
    public void rangerLevelUp_validInputs_shouldReturnIncreasedLevel() {
        //Arrange
        int expected = 2;
        Hero ranger = new Ranger("Renate");
        //Act
        ranger.levelUp();
        int actual = ranger.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void ranger_validInputs_shouldReturnHeroAttributes(){
        //Arrange
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;
        Hero ranger = new Ranger("Renate");
        //Act
        int actualStrength = ranger.getLevelAttributes().getStrength();
        int actualDexterity = ranger.getLevelAttributes().getDexterity();
        int actualIntelligence = ranger.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    @Test
    public void rangerLevelUp_validInputs_shouldReturnIncreasedHeroAttributes(){
        //Arrange
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;
        Hero ranger = new Ranger("Renate");
        //Act
        ranger.levelUp();
        int actualStrength = ranger.getLevelAttributes().getStrength();
        int actualDexterity = ranger.getLevelAttributes().getDexterity();
        int actualIntelligence = ranger.getLevelAttributes().getIntelligence();
        //Assert
        assertAll(
                () -> assertEquals(expectedDexterity, actualDexterity),
                () -> assertEquals(expectedIntelligence, actualIntelligence),
                () -> assertEquals(expectedStrength, actualStrength)
        );
    }

    //Equipping Armor
    @Test
    public void rangerEquipArmor_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero ranger = new Ranger("Renate");
            Armor armor = new Armor("Nice biker", ArmorType.LEATHER, 3,
                    Slot.BODY, new HeroAttributes(1,2,3));
            ranger.equipArmor(armor);
        });
    }

    @Test
    public void rangerEquipArmor_validLevel_shouldNotThrowException() {
        try{
            Hero ranger = new Ranger("Renate");
            Armor armor = new Armor("mmm leather", ArmorType.LEATHER, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            ranger.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void rangerEquipArmor_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidArmorException.class, () -> {
            Hero ranger = new Ranger("Renate");
            Armor armor = new Armor("Frantic Hoodie", ArmorType.CLOTH, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            ranger.equipArmor(armor);
        });
    }

    @Test
    public void rangerEquipArmor_validType_shouldNotThrowException() {
        try{
            Hero ranger = new Ranger("Renate");
            Armor armor = new Armor("mmm leather", ArmorType.LEATHER, 1,
                    Slot.BODY, new HeroAttributes(1,2,3));
            ranger.equipArmor(armor);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    //Equipping weapon
    @Test
    public void rangerEquipWeapon_invalidLevel_shouldReturnExceptionForLevel() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero ranger = new Ranger("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.BOW, 3, 10);
            ranger.equipWeapon(weapon);
        });
    }

    @Test
    public void rangerEquipWeapon_validLevel_shouldNotThrowException() {
        try{
            Hero ranger = new Ranger("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.BOW, 1, 10);
            ranger.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    public void rangerEquipWeapon_invalidType_shouldReturnExceptionForType() {
        assertThrows(InvalidWeaponException.class, () -> {
            Hero ranger = new Ranger("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.HAMMER, 1, 10);
            ranger.equipWeapon(weapon);
        });
    }

    @Test
    public void rangerEquipWeapon_validType_shouldNotThrowException() {
        try{
            Hero ranger = new Ranger("Renate");
            Weapon weapon = new Weapon("Frantic Hoodie", WeaponType.BOW, 1, 10);
            ranger.equipWeapon(weapon);
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }
}
