package hero;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DamageTest {
    @Test
    public void damage_noEquipment_shouldReturnDamageOne() {
        Hero mage = new Mage("Renate");
        float expected = 1;
        float actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damage_withWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero mage = new Mage("Renate");
        Weapon weapon = new Weapon("Lil Wand", WeaponType.WAND,
                1, 1);
        mage.equipWeapon(weapon);
        float expected = 1F*(1F+ 8F/100F);
        float actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damage_replaceWeapon_shouldReturnTotalDamage() throws InvalidWeaponException {
        Hero mage = new Mage("Renate");
        Weapon weapon = new Weapon("Lil Wand", WeaponType.WAND,
                1, 1);
        Weapon anotherweapon = new Weapon("Big Wand", WeaponType.STAFF,
                1, 3);
        mage.equipWeapon(weapon);
        mage.equipWeapon(anotherweapon);

        float expected = 3F*(1F+ 8F/100F);
        float actual = mage.damage();

        assertEquals(expected, actual);
    }

    @Test
    public void damage_equipWeaponAndArmor_shouldReturnTotalDamage() throws InvalidWeaponException, InvalidArmorException {
        Hero mage = new Mage("Renate");
        Weapon weapon = new Weapon("Lil Wand", WeaponType.WAND,
                1, 1);
        Armor armor = new Armor("Crazy Cloth", ArmorType.CLOTH, 1,
                Slot.BODY, new HeroAttributes(1,2,3));
        mage.equipWeapon(weapon);
        mage.equipArmor(armor);

        float expected = 1F*(1F+ 11F/100F);
        float actual = mage.damage();

        assertEquals(expected, actual);
    }
}
