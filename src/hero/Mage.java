package hero;

import equipment.*;
import java.util.ArrayList;

public class Mage extends Hero {
    public Mage(String name){
        super(name);
        levelAttributes = new HeroAttributes(1,1,8);
        levelValues = new HeroAttributes(1,1,5);
        setValidWeaponTypes();
        setValidArmorTypes();
    }

    public void setValidWeaponTypes() {
        ArrayList<WeaponType> wt = new ArrayList<WeaponType>();
        wt.add(WeaponType.STAFF);
        wt.add(WeaponType.WAND);
        setValidWeaponTypes(wt);

    }

    public void setValidArmorTypes() {
        ArrayList<ArmorType> at = new ArrayList<ArmorType>();
        at.add(ArmorType.CLOTH);
        setValidArmorTypes(at);
    }

}
