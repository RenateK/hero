package hero;

import equipment.*;
import java.util.ArrayList;

public class Warrior extends Hero {
    public Warrior(String name){
        super(name);
        levelAttributes = new HeroAttributes(5,2,1);
        levelValues = new HeroAttributes(3,2,1);
        setValidWeaponTypes();
        setValidArmorTypes();
    }

    public void setValidWeaponTypes() {
        ArrayList<WeaponType> wt = new ArrayList<WeaponType>();
        wt.add(WeaponType.AXE);
        wt.add(WeaponType.HAMMER);
        wt.add(WeaponType.STAFF);
        setValidWeaponTypes(wt);
    }

    public void setValidArmorTypes() {
        ArrayList<ArmorType> at = new ArrayList<ArmorType>();
        at.add(ArmorType.PLATE);
        at.add(ArmorType.MAIL);
        setValidArmorTypes(at);
    }

}
