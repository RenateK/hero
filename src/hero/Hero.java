package hero;

import equipment.*;
import exceptions.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {
    private String name;
    private int level;
    protected HeroAttributes levelAttributes;
    protected HeroAttributes levelValues;
    private ArrayList<WeaponType> validWeaponTypes;
    private ArrayList<ArmorType> validArmorTypes;
    private HashMap<Slot, Item> equipment = new HashMap<Slot, Item>();

    //Constructor
    public Hero(String name){
        this.name = name;
        this.level = 1;
    }

    public void levelUp(){
        level++;
        levelAttributes.increaseLevelAttributes(levelValues);
    }

    public String displayHero(){
        StringBuilder stb = new StringBuilder();
        stb.append("Name: ");
        stb.append(getName());
        stb.append("\n");
        stb.append("Class: ");
        stb.append(getClass().getSimpleName());
        stb.append("\n");
        stb.append("Level: ");
        stb.append(getLevel());
        stb.append("\n");
        stb.append("Strength: ");
        stb.append(getLevelAttributes().getStrength());
        stb.append("\n");
        stb.append("Dexterity: ");
        stb.append(getLevelAttributes().getDexterity());
        stb.append("\n");
        stb.append("Intelligence: ");
        stb.append(getLevelAttributes().getIntelligence());
        stb.append("\n");
        stb.append("Damage: ");
        stb.append(damage());
        return stb.toString();
        // Is this correct use of StringBuilder?
    }
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {

        if (level >= weapon.getRequiredLevel() && validWeaponTypes.contains(weapon.getWeaponType())) {
            equipment.put(Slot.WEAPON, weapon);
        } else {
            throw new InvalidWeaponException("Can not equip weapon!");
        }
    }

    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (level >= armor.getRequiredLevel() && validArmorTypes.contains(armor.getArmorType())) {
            equipment.put(armor.getSlot(), armor);
        } else {
            throw new InvalidArmorException("Can not equip armor!");
        }
    }

    public List<Float> totalAttributes() {
        List<Float> totalAttributes = new ArrayList<Float>();
        float totalStrength = levelAttributes.getStrength();
        float totalDexterity = levelAttributes.getDexterity();
        float totalIntelligence = levelAttributes.getIntelligence();
        for (Map.Entry<Slot, Item> value : equipment.entrySet()) {
            if ((value.getKey() != Slot.WEAPON) && (value.getValue() != null)) {
                totalStrength += ((Armor)value.getValue()).getArmorAttribute().getStrength();
                totalDexterity += ((Armor)value.getValue()).getArmorAttribute().getDexterity();
                totalIntelligence += ((Armor)value.getValue()).getArmorAttribute().getIntelligence();
            }
        }
        totalAttributes.add(totalStrength);
        totalAttributes.add(totalDexterity);
        totalAttributes.add(totalIntelligence);
        return totalAttributes;
    }

    public float damage() {
        float totalStrength = totalAttributes().get(0);
        float totalDexterity = totalAttributes().get(1);
        float totalIntelligence = totalAttributes().get(2);
        float damagingAttribute = 0;
        if (equipment.get(Slot.WEAPON) == null){
            damagingAttribute = 1;
        } else if(this.getClass()==Mage.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalIntelligence/100);
        } else if(this.getClass()==Ranger.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalDexterity/100);
        } else if(this.getClass()==Rogue.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalDexterity/100);
        } else if(this.getClass()==Warrior.class) {
            damagingAttribute = ((Weapon)equipment.get(Slot.WEAPON)).getWeaponDamage() * (1+ totalStrength/100);
        }
        return damagingAttribute;
    }

    //Getters and setters
    public String getName() {
        return name;
    }
    public int getLevel() {
        return level;
    }
    public HeroAttributes getLevelAttributes() {
        return levelAttributes;
    }
    public void setValidWeaponTypes(ArrayList<WeaponType> validWeaponTypes) {
        this.validWeaponTypes = validWeaponTypes;
    }
    public void setValidArmorTypes(ArrayList<ArmorType> validArmorTypes) {
        this.validArmorTypes = validArmorTypes;
    }
    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }
}
