package hero;

import equipment.*;
import java.util.ArrayList;

public class Rogue extends Hero {
    public Rogue(String name){
        super(name);
        levelAttributes = new HeroAttributes(2,6,1);
        levelValues = new HeroAttributes(1,4,1);
        setValidWeaponTypes();
        setValidArmorTypes();
    }

    public void setValidWeaponTypes() {
        ArrayList<WeaponType> wt = new ArrayList<WeaponType>();
        wt.add(WeaponType.DAGGER);
        wt.add(WeaponType.SWORD);
        setValidWeaponTypes(wt);
    }

    public void setValidArmorTypes() {
        ArrayList<ArmorType> at = new ArrayList<ArmorType>();
        at.add(ArmorType.LEATHER);
        at.add(ArmorType.MAIL);
        setValidArmorTypes(at);
    }

}
