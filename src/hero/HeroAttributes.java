package hero;

public class HeroAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public HeroAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public void increaseLevelAttributes(HeroAttributes levelValues) {
        this.strength += levelValues.strength;
        this.dexterity += levelValues.dexterity;
        this.intelligence += levelValues.intelligence;
    }

    //Getters
    public int getStrength() { return strength; }
    public int getDexterity(){ return dexterity; }
    public int getIntelligence(){ return intelligence; }

}
