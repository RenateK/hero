package hero;

import equipment.*;
import java.util.ArrayList;

public class Ranger extends Hero {
    public Ranger(String name){
        super(name);
        levelAttributes = new HeroAttributes(1,7,1);
        levelValues = new HeroAttributes(1,5,1);
        setValidWeaponTypes();
        setValidArmorTypes();
    }

    public void setValidWeaponTypes() {
        ArrayList<WeaponType> wt = new ArrayList<WeaponType>();
        wt.add(WeaponType.BOW);
        setValidWeaponTypes(wt);
    }

    public void setValidArmorTypes() {
        ArrayList<ArmorType> at = new ArrayList<ArmorType>();
        at.add(ArmorType.LEATHER);
        at.add(ArmorType.MAIL);
        setValidArmorTypes(at);
    }

}
