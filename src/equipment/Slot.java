package equipment;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
