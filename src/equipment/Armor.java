package equipment;

import hero.HeroAttributes;

public class Armor extends Item{
    private final ArmorType armorType;
    private HeroAttributes armorAttribute;

    public Armor(String name, ArmorType armorType, int requiredLevel, Slot slot, HeroAttributes armorAttribute) {
        super(name, requiredLevel);
        this.armorType = armorType;
        this.slot = slot;
        this.armorAttribute = armorAttribute;

    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public HeroAttributes getArmorAttribute() {
        return armorAttribute;
    }


}
