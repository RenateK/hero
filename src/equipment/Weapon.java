package equipment;

public class Weapon extends Item {
    private final WeaponType weaponType;
    private int weaponDamage;

    public Weapon(String name, WeaponType weaponType, int requiredLevel, int weaponDamage) {
        super(name, requiredLevel);
        this.weaponType = weaponType;
        this.slot = Slot.WEAPON;
        this.weaponDamage = weaponDamage;

    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
    public int getWeaponDamage() {
        return weaponDamage;
    }
}
